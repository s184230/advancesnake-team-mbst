package resources.model;

import java.awt.*;
import java.util.Random;

public class Apple {
    private final Point pos;
    private final int x;
    private final int y;
    private final Snake snek;

    public Apple(int gx, int gy, Snake snek) {
        this.x = gx;
        this.y = gy;
        this.snek = snek;
        int tx;
        int ty;
        do {
            Random rnd = new Random();

            tx = rnd.nextInt(this.x);
            ty = rnd.nextInt(this.y);
        } while (tx == x / 2 && ty == y / 2 || tx == x / 2 + 1 && ty == y / 2);
        pos = new Point(tx,ty);
    }

    public void newApple() {
        // TODO: tjek om position er på slangen
        int tx;
        int ty;
        do {
            Random rnd = new Random();

            tx = rnd.nextInt(this.x);
            ty = rnd.nextInt(this.y);
        } while (snek.getHead().equals(new Point(tx, ty)) || snek.getBody().contains(new Point(tx, ty)));
        pos.setLocation(tx, ty);
    }

    public Point getPos() {
        return this.pos;
    }
}
