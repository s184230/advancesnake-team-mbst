package resources.model;

import java.awt.*;
import java.util.ArrayList;


/*  Denne klasse indeholder information om Snake
    samt metoder til at flytte rundt, tjekke
    om den har ramt sig selv eller om den er uden for gitteret
 */

public class Snake {
    private final Point head = new Point();
    private final ArrayList<Point> body = new ArrayList<>();
    private final int gx;
    private final int gy;

    public Snake(int x, int y) {
        this.head.setLocation(x / 2, y / 2);
        this.body.add(new Point(x / 2 + 1, y / 2));
        this.gx = x;
        this.gy = y;
    }
    // Flytter slangen baseret på retning
    public void move(char dir) {

        // body ArrayListen skal nu også indeholde
        body.add(new Point(head));

        switch (dir) {
            case 'U':
                head.translate(0, -1);
                if (borderControl()) {
                    head.setLocation(head.getX(), gy -1);
                }
                break;

            case 'D':
                head.translate(0, 1);
                if (borderControl()) {
                    head.setLocation(head.getX(), 0);
                }
                break;

            case 'L':
               head.translate(-1, 0);
                if (borderControl()) {
                    head.setLocation(gx-1, head.getY());
                }
                break;

            case 'R':
                head.translate(1, 0);
                if (borderControl()) {
                    head.setLocation(0, head.getY());
                }
                break;
        }
    }

    public boolean ateApple(Apple apple) {
        if (!head.equals(apple.getPos())) {
            body.remove(0);
        }
        return head.equals(apple.getPos());
    }

    private boolean borderControl() {
        return head.getX() < 0 || head.getY() < 0 || head.getX() >= gx || head.getY() >= gy;
    }

    public boolean hitSelf() {
        // hvis head er i body, så er det no good
        return body.contains(head);
    }

    public ArrayList<Point> getBody() {
        return new ArrayList<>(body);
    }
    public Point getHead() {
        return new Point(head);
    }
}
