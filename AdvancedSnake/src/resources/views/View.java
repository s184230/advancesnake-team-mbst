package resources.views;

import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import resources.Main;
import resources.model.Apple;
import resources.model.Snake;

import java.awt.*;


public class View {
    public Canvas gridCanvas;
    public GraphicsContext gc;

    private final Snake snek;
    private final Apple apple;

    public View(Snake snek, Apple apple) {
        this.snek = snek;
        this.apple = apple;
    }

    public void makeScene(Scene scene) {
        gridCanvas = new Canvas(scene.getWidth(), scene.getHeight());
        gc = gridCanvas.getGraphicsContext2D();
        drawGrid(apple, snek, gc);
    }

    public void drawGrid(Apple apple, Snake snek, GraphicsContext gc) {
        for (int i = 0; i < Main.X_VALUE; i++) {
            for (int j = 0; j < Main.Y_VALUE; j++) {
                Point p = new Point(i, j);

                gc.beginPath();
                gc.setLineWidth(0.5);
                gc.setStroke(Color.WHITE);
                gc.setFill(Color.WHITE);
                if (snek.getHead().equals(p)) {
                    gc.setStroke(Color.DARKGREEN);
                    gc.setFill(Color.DARKGREEN);
                } else if (snek.getBody().contains(p)) {
                    gc.setStroke(Color.GREEN);
                    gc.setFill(Color.GREEN);
                }
                if (apple.getPos().equals(p)) {
                    gc.setStroke(Color.WHITE);
                    gc.setFill(Color.RED);
                }

                gc.strokeRect(i * Main.MULTIPLIER, j * Main.MULTIPLIER, Main.MULTIPLIER, Main.MULTIPLIER);
                gc.fillRect(i * Main.MULTIPLIER, j * Main.MULTIPLIER, Main.MULTIPLIER, Main.MULTIPLIER);
                gc.closePath();
            }
        }
    }
}
