package resources;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import resources.model.Apple;
import resources.model.Snake;
import resources.views.View;

public class Main extends Application {
    //X, Y_Value styrer antallet af tern i scene
    //MULTIPLIYER styrer størrelsen af tern
    public static int X_VALUE = 20;
    public static int Y_VALUE = 20;
    public final static int MULTIPLIER = 20;

    @Override
    public void start(Stage primaryStage) {
        //Laver slange, æble
        Snake snek = new Snake(X_VALUE, Y_VALUE);
        Apple apple = new Apple(X_VALUE, Y_VALUE, snek);

        //Instanser af view og controller, der har de samme instanser af snake og apple.
        View view = new View(snek, apple);
        Controller controller = new Controller(snek, apple, view);

        Pane root = new Pane();
        Scene scene = new Scene(root, X_VALUE * MULTIPLIER, Y_VALUE * MULTIPLIER);

        //Styrer on KeyPress
        scene.setOnKeyPressed(controller::handle);

        //Laver scene og tilføjer canvas.
        view.makeScene(scene);
        root.getChildren().addAll(view.gridCanvas);

        primaryStage.setTitle("gaem of snek");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        setGridSize(args);
        launch(args);
    }

    // Sætter gittersize såfremt at de givne parametre overholder 5 <= arg <= 100
    private static void setGridSize(String[] args) {
        boolean validGridSize = false;
        if (args.length == 2) {
            validGridSize = true;
            for (int i = 0; i < 2; i++) {
                if (((Integer.parseInt(args[i]) < 5) || (Integer.parseInt(args[i]) > 100))) {
                    validGridSize = false;
                }
            }
        }

        if (validGridSize) {
            X_VALUE = Integer.parseInt(args[0]);
            Y_VALUE = Integer.parseInt(args[1]);
        }
    }

}
