package resources;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import resources.model.Apple;
import resources.model.Snake;
import resources.views.View;


// Controller klasse some styrer hvad der sker med view, snake og apple
// undervejs i spillet. Denne har også TimeLine i sig, som looper spillet.
class Controller {
    private Snake snek;
    private Apple apple;
    private View view;
    private KeyCode key = KeyCode.LEFT;
    private boolean keyPressed = false;

    public Controller(Snake snek, Apple apple, View view) {
        this.snek = snek;
        this.apple = apple;
        this.view = view;

        animation.setCycleCount(Animation.INDEFINITE);
        double SPEED = 10;
        animation.setRate(SPEED);
    }

    // TimeLine som looper spillet
    private final Timeline animation = new Timeline(new KeyFrame(Duration.seconds(1), event -> {

        if (snek.hitSelf()) return;
        if (key.equals(KeyCode.LEFT)) {
            snek.move('L');
        } else if (key.equals(KeyCode.RIGHT)) {
            snek.move('R');
        } else if (key.equals(KeyCode.UP)) {
            snek.move('U');
        } else if (key.equals(KeyCode.DOWN)) {
            snek.move('D');
        }
        if (snek.ateApple(apple)) {
            apple.newApple();
        }

        view.drawGrid(apple, snek, view.gc);

        keyPressed = false;
    }));


    // handler som tjekker hvilke taster der trykkes og hvad der skal gøres
    public void handle(KeyEvent event) {
        if (event.getCode().isArrowKey()) animation.play();

        if (valKey(event.getCode()) && !keyPressed) {
            key = event.getCode();
            keyPressed = true;

            view.drawGrid(apple, snek, view.gc);
        }

        // Spillet pauses hvis 'P' trykkes på tastaturet
        if (event.getCode().equals(KeyCode.P)) {
            animation.stop();
        }

        // Spillet genstartes hvis 'ENTER' trykkes på tastaturet
        if (event.getCode().equals(KeyCode.ENTER)) {
            reset();
        }
    }

    // returnerer true hvis parametren key en piletast og ikke er
    // lig sin modsatte tast (OP vs NED, HØJRE vs VENSTRE)
    private boolean valKey(KeyCode key) {
        if (!key.isArrowKey()) return false;
        else {
            return (key.equals(KeyCode.LEFT) && !this.key.equals(KeyCode.RIGHT) || key.equals(KeyCode.RIGHT) && !this.key.equals(KeyCode.LEFT) || key.equals(KeyCode.DOWN) && !this.key.equals(KeyCode.UP) || key.equals(KeyCode.UP) && !this.key.equals(KeyCode.DOWN));
        }
    }

    // resetter spillet
    private void reset() {

        // Går tilbage til starten af animation TimeLinen og stopper den bagefter
        animation.jumpTo("start");
        animation.stop();

        // Laver ny snek og apple, som de er helt fra start af
        snek = new Snake(Main.X_VALUE, Main.Y_VALUE);
        apple = new Apple(Main.X_VALUE, Main.Y_VALUE, snek);

        // Sætter forskellige fields til deres default-værdier
        key = KeyCode.LEFT;
        keyPressed = false;

        // Tegner canvasset igen
        view.drawGrid(apple, snek, view.gc);
    }
}
